
// Project name (artifact name in Maven)
name := "trianglepaths"

// orgnization name (e.g., the package name of the project)
organization := "it.manse.trianglepaths"

version := "1.0-SNAPSHOT"

// project description
description := "SuprNation challenge"

mainClass in (Compile, run) := Some("it.manse.trianglepaths.Application")

// library dependencies. (orginization name) % (project name) % (version)
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"
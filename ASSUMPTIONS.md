1. The structure is depicted as a tree since the nodes with common parents are splitted up into two nodes (remember that a structure is a tree if does not exist a node with two different parents).
2. Even though the structure is a graph, I preferred to use the tree structure since it has an head and leaf nodes. Graphs does not have leaves and I need to compute fixed paths.
3. Since the problem is a well-know problem (tree traversal) I will use as resolver a depth-first search because it is recursive instead of the other method breadth-first search.
4. The input cannot contain holes in lines (this isn't tested since not in scope, but obvisiusly it should be)
5. I'm using reverse list operation, since it's required that the algorithm works at most with 500 hundreds of rows
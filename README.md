# TrianglePaths

The application could be run using sbt with this syntax:

`$ cat << EOF | sbt run`\
`> 7`\
`> 6 3`\
`> 3 8 5`\
`> 11 2 10 9`\
`> EOF`


You may want run also tests with:

`sbt test` 
package it.manse.trianglepaths.data

class TreeBuilder {

  def build(treeS: List[List[Int]]) : TreeNode = {

    val size = treeS.size - 1
    def iter(ii: Int, jj: Int) : TreeNode = {
      if (ii == size) {
        Leaf(treeS(ii)(jj))
      } else {
        Subtree(treeS(ii)(jj), iter(ii + 1, jj), iter(ii + 1, jj + 1))
      }
    }

    iter(0, 0)
  }

}

package it.manse.trianglepaths.data

import scala.annotation.tailrec

trait TreeNode {
  def depth: Integer
}
case class Subtree(head: Int, left: TreeNode, right: TreeNode) extends TreeNode {
  override def toString: String = s"$left $head $right"

  override def depth: Integer = {
    @tailrec
    def iter(tree: TreeNode, acc: Integer) : Integer = {
      tree match {
        case t: Subtree => iter(t.left, acc + 1)
        case l: Leaf => acc + l.depth
      }
    }
    iter(this.left, 1)
  }
}
case class Leaf(head: Int) extends TreeNode {
  override def toString: String = s"[$head]"
  override def depth: Integer = 1
}

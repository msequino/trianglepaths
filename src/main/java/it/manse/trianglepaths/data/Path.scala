package it.manse.trianglepaths.data

class Path(val steps: List[Int], val total: Int) {
  override def toString: String = {
    s"${steps.mkString(" + ")} = $total"
  }
}
object Path {
  def min(pathA: Path, pathB: Path) : Path = if(pathA.total <= pathB.total) pathA else pathB
}

package it.manse.trianglepaths

import it.manse.trianglepaths.algorithm.{Algorithms, AlgorithmsExecutor}
import it.manse.trianglepaths.data.TreeBuilder

import scala.annotation.tailrec

object Application {

  def main1(args: Array[String]): Unit = {

    val input: List[List[Int]] = io.Source.stdin.getLines.toList.map(ln => ln.split(" ").toList.map(nm => Integer.parseInt(nm)))
    val tree = new TreeBuilder().build(input)
    val minimalPath = AlgorithmsExecutor.compute(tree)(Algorithms.findMinimalPath)
    println(s"Minimal path is: $minimalPath")

  }

  def main(args: Array[String]): Unit = {

    val text = "7\n" +
    "6 3\n" +
    "3 8 5\n" +
    "11 2 10 1\n"

    val input: List[List[Int]] = text.split("\n").toList.map(ln => ln.split(" ").toList.map(nm => Integer.parseInt(nm)))

    /*val depth = 500
    val input : List[List[Int]] = (for(i <- 1 to depth) yield List.fill(i)(i)).toList
*/
    def findBest(current: Int, pos: Int, bottom: (Int, Int) ) : (Int, Int) = (current + Math.min(bottom._1, bottom._2) , pos + (if(bottom._1 < bottom._2) 0 else 1))

    @tailrec
    def iter(currentIndex: Int, memo: Seq[Int], path: List[Int]) : (List[Int], Int) = {
      val bestValuesWithIndex = for (col <- 0 to currentIndex) yield findBest(input(currentIndex)(col), col, (memo(col), memo(col+1)))
      val minimumValue: (Int, Int) = bestValuesWithIndex.minBy(_._1)
      if(currentIndex == 0) {
        (input.head.head :: input(currentIndex + 1)(minimumValue._2) :: path, memo.head + input.head.head)
      } else {
        iter(currentIndex - 1, bestValuesWithIndex.map(_._1), input(currentIndex + 1)(minimumValue._2) :: path)
      }
    }

    val result = iter(input.size - 2, input.last, List.empty)
    println(s"Minimum path value is ${result._1.mkString(" + ") + " = " +result._2}")
  }
}

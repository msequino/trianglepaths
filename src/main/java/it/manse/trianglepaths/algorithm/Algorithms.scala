package it.manse.trianglepaths.algorithm

import it.manse.trianglepaths.data.{Leaf, Path, Subtree, TreeNode}

object Algorithms {
  def findMinimalPath(pathA: Path, pathB: Path): Path = if(pathA.total <= pathB.total) pathA else pathB
}

object AlgorithmsExecutor {

  def compute(tree: TreeNode)(algorithmResolver: (Path, Path) => Path): Path = {
    def iter(treeNode: TreeNode, currPath: Path, bestPath: Path): Path = {
      treeNode match {
        case Subtree(v, left, right) =>
          val pt = new Path(currPath.steps :+ v, currPath.total + v)
          if(bestPath.total <= pt.total) bestPath
          else algorithmResolver(iter(left, pt, bestPath), iter(right, pt, bestPath))

        case Leaf(v) => if(currPath.total + v < bestPath.total) new Path(currPath.steps :+ v, currPath.total + v) else bestPath
      }
    }
    iter(tree, new Path(List.empty, 0), new Path(List.empty, Integer.MAX_VALUE))
  }
}

package it.manse.trianglepaths.algorithm

import it.manse.trianglepaths.data.{Leaf, Path, Subtree, TreeBuilder}
import org.scalatest.FunSuite

class AlgorithmsTest extends FunSuite {

  private val runningAlgoritms : (Path, Path) => Path = Algorithms.findMinimalPath

  test("testCompute - find minimal path known tree") {
    val tree = new Subtree(7,
      Subtree(6,
        Subtree(3,
          Leaf(11),
          Leaf(2)),
        Subtree(8,
          Leaf(2),
          Leaf(10))),
      Subtree(3,
        Subtree(8,
          Leaf(2),
          Leaf(10)),
        Subtree(5,
          Leaf(10),
          Leaf(9))))

    val minimalPath = AlgorithmsExecutor.compute(tree)(runningAlgoritms)
    println(minimalPath)
    assert(minimalPath.total == 18)
  }

  test("testCompute - find minimal path of changed tree") {
    val tree = new Subtree(7,
      Subtree(6,
        Subtree(3,
          Leaf(11),
          Leaf(2)),
        Subtree(2,
          Leaf(2),
          Leaf(10))),
      Subtree(3,
        Subtree(2,
          Leaf(2),
          Leaf(10)),
        Subtree(5,
          Leaf(10),
          Leaf(9))))

    val minimalPath = AlgorithmsExecutor.compute(tree)(runningAlgoritms)
    println(minimalPath)
    assert(minimalPath.total == 14)
  }
  test("testBuildTree - 5 rows") {

    val depth = 5
    val input : List[List[Int]] = (for(i <- 1 to depth) yield List.fill(i)(i)).toList

    val s1 = System.currentTimeMillis()
    val comp = new TreeBuilder().build(input)
    println(s"execute parsing ${System.currentTimeMillis() - s1}")
    val s2 = System.currentTimeMillis()
    val minimalPath = AlgorithmsExecutor.compute(comp)(runningAlgoritms)
    println(s"execute algorithm ${System.currentTimeMillis() - s2}")
    println(minimalPath)
    assert(minimalPath.total == 15)

  }
  test("testBuildTree - 50 rows") {

    val depth = 50
    val input : List[List[Int]] = (for(i <- 1 to depth) yield List.fill(i)(i)).toList

    println("starting")
    val comp = new TreeBuilder().build(input)
        println(comp)
    val minimalPath = AlgorithmsExecutor.compute(comp)(runningAlgoritms)
    println(minimalPath)
    assert(minimalPath.total == 1)

  }
  test("testBuildTree - 500 rows") {

    val depth = 500
    val input : List[List[Int]] = (for(i <- 1 to depth) yield List.fill(i)(i)).toList

    val comp = new TreeBuilder().build(input)
//    println(comp)
    val minimalPath = AlgorithmsExecutor.compute(comp)(runningAlgoritms)
    println(minimalPath)
    assert(minimalPath.total == 14)

  }

  test("testBuildTree - 1000 rows") {

    val depth = 1000
    val input : List[List[Int]] = (for(i <- 1 to depth) yield List.fill(i)(i)).toList

    val comp = new TreeBuilder().build(input)
//    println(comp)
    val minimalPath = AlgorithmsExecutor.compute(comp)(runningAlgoritms)
    println(minimalPath)

    assert(minimalPath.total == 14)
  }
}

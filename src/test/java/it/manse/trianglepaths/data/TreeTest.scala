package it.manse.trianglepaths.data

import org.scalatest.{BeforeAndAfterEach, FunSuite}

class TreeTest extends FunSuite with BeforeAndAfterEach {

  test("testBuildTree") {
    val input : List[List[Int]] = List(
      List(7),
      List(6, 3),
      List(3, 8 ,5),
      List(11, 2, 10, 9)
    )

    val tree = new Subtree(7,
      Subtree(6,
        Subtree(3,
          Leaf(11),
          Leaf(2)),
        Subtree(8,
          Leaf(2),
          Leaf(10))),
      Subtree(3,
        Subtree(8,
          Leaf(2),
          Leaf(10)),
        Subtree(5,
          Leaf(10),
          Leaf(9))))

    val comp = new TreeBuilder().build(input)
    println(comp)
    assert(comp.depth == 4)

  }

  test("testBuildTree - 500 rows") {

    val depth = 500
    val input : List[List[Int]] = (for(i <- 1 to depth) yield List.fill(i)(i)).toList

    val comp = new TreeBuilder().build(input)
    assert(comp.depth == depth)

  }

  test("testBuildTree - 1000 rows") {

    val depth = 1000
    val input : List[List[Int]] = (for(i <- 1 to depth) yield List.fill(i)(i)).toList

    val comp = new TreeBuilder().build(input)
    assert(comp.depth == depth)

  }
}
